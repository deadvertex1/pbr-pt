use crate::vec3::Vec3;
use rand::rngs::ThreadRng;
use rand::Rng;
use std::f32::consts::PI;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum Axis {
    X,
    Y,
    Z,
}

pub fn find_most_significant_axis(v: Vec3) -> Axis {
    let x = v.x.abs();
    let y = v.y.abs();
    let z = v.z.abs();
    if x > y && x > z {
        Axis::X
    } else if y > x && y > z {
        Axis::Y
    } else {
        Axis::Z
    }
}

// RETURNS: (azimuth, inclination)
pub fn map_cartesian_to_spherical_coordinates(v: Vec3) -> (f32, f32) {
    let n = Vec3::normalize(v);

    let inclination = f32::atan2(f32::sqrt(n.x * n.x + n.z * n.z), n.y);
    let azimuth = f32::atan2(n.z, n.x);

    (azimuth, inclination)
}

pub fn map_spherical_to_equirectangular_coordinates(sx: f32, sy: f32) -> (f32, f32) {
    // Map sx from -PI..PI to 0..1
    let u = (sx / PI) * 0.5 + 0.5;
    let v = f32::cos(sy) * 0.5 + 0.5;

    (u, v)
}

pub fn map_equirectangular_to_spherical_coordinates(u: f32, v: f32) -> (f32, f32) {
    // Map V from 0..1 to -1..1 range
    let a = v * 2.0 - 1.0;
    let sphere_y = f32::acos(a);

    // Map U from 0..1 to -PI to PI range
    let sphere_x = (u * 2.0 - 1.0) * PI;

    (sphere_x, sphere_y)
}

pub fn map_spherical_to_cartesian_coordinates(sx: f32, sy: f32) -> Vec3 {
    Vec3 {
        x: f32::sin(sy) * f32::cos(sx),
        z: f32::sin(sy) * f32::sin(sx),
        y: f32::cos(sy),
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use all_asserts::assert_near;

    #[test]
    fn test_find_most_significant_axis() {
        assert_eq!(
            find_most_significant_axis(Vec3::new(1.0, 0.0, 0.0)),
            Axis::X
        );
        assert_eq!(
            find_most_significant_axis(Vec3::new(-1.0, 0.0, 0.0)),
            Axis::X
        );
        assert_eq!(
            find_most_significant_axis(Vec3::new(0.0, -1.0, 0.0)),
            Axis::Y
        );
        assert_eq!(
            find_most_significant_axis(Vec3::new(1.0, 2.0, -3.0)),
            Axis::Z
        );
    }

    #[test]
    fn test_map_cartesian_to_spherical_coordinates() {
        assert_eq!(
            map_cartesian_to_spherical_coordinates(Vec3::new(0.0, 1.0, 0.0)),
            (0.0, 0.0)
        );
        assert_eq!(
            map_cartesian_to_spherical_coordinates(Vec3::new(1.0, 0.0, 0.0)),
            (0.0, 0.5 * PI)
        );
        assert_eq!(
            map_cartesian_to_spherical_coordinates(Vec3::new(0.0, 0.0, -1.0)),
            (-0.5 * PI, 0.5 * PI)
        );
    }

    #[test]
    fn test_map_spherical_to_equirectangular_coordinates() {
        // +Y
        assert_eq!(
            map_spherical_to_equirectangular_coordinates(0.0, 0.0),
            (0.5, 1.0)
        );

        // +X
        // TODO: Need to double check this is correct, this suggests that the center pixel of an
        // equirectangular image is +X vector
        let (u, v) = map_spherical_to_equirectangular_coordinates(0.0, 0.5 * PI);
        assert_near!(u, 0.5, f32::EPSILON);
        assert_near!(v, 0.5, f32::EPSILON);

        // +Z
        let (u, v) = map_spherical_to_equirectangular_coordinates(0.5 * PI, 0.5 * PI);
        assert_near!(u, 0.75, f32::EPSILON); // TODO: X and Z axis seem flipped compared to Blender
        assert_near!(v, 0.5, f32::EPSILON);
    }
}

pub fn random_in_unit_sphere(rng: &mut ThreadRng) -> Vec3 {
    let mut p: Vec3;
    loop {
        p = Vec3::new(
            rng.gen_range(0.0..1.0),
            rng.gen_range(0.0..1.0),
            rng.gen_range(0.0..1.0),
        ) * 2.0
            - Vec3::new(1.0, 1.0, 1.0);
        if Vec3::dot(p, p) >= 1.0 {
            break;
        }
    }
    p
}

pub fn random_in_unit_disk(rng: &mut ThreadRng) -> Vec3 {
    let mut p: Vec3;
    loop {
        p = 2.0 * Vec3::new(rng.gen_range(0.0..1.0), rng.gen_range(0.0..1.0), 0.0)
            - Vec3::new(1.0, 1.0, 0.0);

        if Vec3::dot(p, p) >= 1.0 {
            break;
        }
    }
    p
}
