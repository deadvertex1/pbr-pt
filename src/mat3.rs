use core::ops::Mul;
use crate::vec3::Vec3;

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Mat3 {
    pub x: Vec3,
    pub y: Vec3,
    pub z: Vec3,
}

impl Mat3 {
    pub fn new(x: Vec3, y: Vec3, z: Vec3) -> Mat3 {
        Self {
            x,
            y,
            z,
        }
    }

    pub fn identity() -> Mat3 {
        Self {
            x: Vec3::new(1.0, 0.0, 0.0),
            y: Vec3::new(0.0, 1.0, 0.0),
            z: Vec3::new(0.0, 0.0, 1.0),
        }
    }

    pub fn transpose(m: Mat3) -> Mat3 {
        Self {
            x: Vec3::new(m.x.x, m.y.x, m.z.x),
            y: Vec3::new(m.x.y, m.y.y, m.z.y),
            z: Vec3::new(m.x.z, m.y.z, m.z.z),
        }
    }

    pub fn build_orthonormal_basis(z: Vec3) -> Mat3 {
        // TODO: Should probably use something more standard like Gram-Schmidt or something for
        // constructing this basis rather than translating old C++ code I had lying around.
        let mut x = -Vec3::cross(Vec3::new(0.0, 1.0, 0.0), z);
        if Vec3::dot(x, x) < f32::EPSILON {
            x = Vec3::cross(z, Vec3::new(1.0, 0.0, 0.0));
            assert!(Vec3::dot(x, x) > f32::EPSILON);
        }
        x = Vec3::normalize(x);
        let y = Vec3::normalize(Vec3::cross(z, x));

        Mat3 {
            x,
            y,
            z,
        }
    }
}

impl Mul<Vec3> for Mat3 {
    type Output = Vec3;

    fn mul(self, v: Vec3) -> Vec3 {
        Vec3 {
            x: Vec3::dot(Vec3::new(self.x.x, self.y.x, self.z.x), v),
            y: Vec3::dot(Vec3::new(self.x.y, self.y.y, self.z.y), v),
            z: Vec3::dot(Vec3::new(self.x.z, self.y.z, self.z.z), v),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test] 
    fn test_mat3_mul() {
        let m = Mat3::new(
            Vec3::new(1.0, 4.0, 7.0),
            Vec3::new(2.0, 5.0, 8.0),
            Vec3::new(3.0, 6.0, 9.0),
        );

        let v = Vec3::new(1.0, 2.0, 3.0);

        /*
        | 1 2 3 |   | 1 |   | 14 |
        | 4 5 6 | X | 2 | = | 32 |
        | 7 8 9 |   | 3 |   | 50 |
        */

        let result = m * v;
        assert_eq!(result.x, 14.0);
        assert_eq!(result.y, 32.0);
        assert_eq!(result.z, 50.0);
    }

    #[test]
    fn test_mat3_transpose() {
        let m = Mat3::new(
            Vec3::new(1.0, 4.0, 7.0),
            Vec3::new(2.0, 5.0, 8.0),
            Vec3::new(3.0, 6.0, 9.0),
        );

        /*
                   | 1 2 3 |     | 1 4 7 |
        transpose( | 4 5 6 | ) = | 2 5 8 |
                   | 7 8 9 |     | 3 6 9 |
        */
        let result = Mat3::transpose(m);
        assert_eq!(result.x, Vec3::new(1.0, 2.0, 3.0));
        assert_eq!(result.y, Vec3::new(4.0, 5.0, 6.0));
        assert_eq!(result.z, Vec3::new(7.0, 8.0, 9.0));
    }

    #[test]
    fn test_mat3_orthonormal_basis() {
        let z = Vec3::new(0.0, 1.0, 0.0);
        let basis = Mat3::build_orthonormal_basis(z);

        assert_eq!(Vec3::dot(basis.x, basis.y), 0.0);
        assert_eq!(Vec3::dot(basis.y, basis.z), 0.0);
        assert_eq!(Vec3::dot(basis.x, basis.z), 0.0);

        assert_eq!(basis.z, Vec3::new(0.0, 1.0, 0.0));
        assert_eq!(basis.x, Vec3::new(0.0, 0.0, -1.0));
        assert_eq!(basis.y, Vec3::new(-1.0, 0.0, 0.0));
    }

    #[test]
    fn test_mat3_orthonormal_basis2() {
        let z = Vec3::new(1.0, 0.0, 0.0);
        let basis = Mat3::build_orthonormal_basis(z);

        assert_eq!(Vec3::dot(basis.x, basis.y), 0.0);
        assert_eq!(Vec3::dot(basis.y, basis.z), 0.0);
        assert_eq!(Vec3::dot(basis.x, basis.z), 0.0);

        assert_eq!(basis.z, Vec3::new(1.0, 0.0, 0.0));
        assert_eq!(basis.x, Vec3::new(0.0, 0.0, 1.0));
        assert_eq!(basis.y, Vec3::new(0.0, -1.0, 0.0));
    }
}
