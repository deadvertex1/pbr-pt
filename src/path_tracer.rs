use image::{Rgb, Rgb32FImage, RgbImage};
use rand::rngs::ThreadRng;
use rand::Rng;
use std::f32::consts::PI;

pub use crate::math_utils::*;
use crate::vec3::Vec3;
use crate::mat3::Mat3;

#[derive(Debug, Copy, Clone, PartialEq)]
enum MaterialType {
    Diffuse,
    Metal,
    Dielectric,
    Microfacet,
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Material {
    base_type: MaterialType,
    albedo: Vec3,
    fuzziness: f32,
    ior: f32,
    sigma: f32, // Oren Nayar
}

impl Material {
    fn new_diffuse(albedo: Vec3) -> Self {
        Self {
            base_type: MaterialType::Diffuse,
            albedo,
            fuzziness: 0.0,
            ior: 0.0,
            sigma: 0.0,
        }
    }

    fn new_metal(albedo: Vec3, fuzziness: f32) -> Self {
        Self {
            base_type: MaterialType::Metal,
            albedo,
            fuzziness,
            ior: 0.0,
            sigma: 0.0,
        }
    }

    fn new_dielectric(ior: f32) -> Self {
        Self {
            base_type: MaterialType::Dielectric,
            albedo: Vec3::new(1.0, 1.0, 1.0),
            fuzziness: 0.0,
            ior,
            sigma: 0.0,
        }
    }

    fn new_oren_nayar(albedo: Vec3, sigma: f32) -> Self {
        Self {
            base_type: MaterialType::Microfacet,
            albedo,
            fuzziness: 0.0,
            ior: 0.0,
            sigma,
        }
    }
}

#[derive(Debug, Copy, Clone, PartialEq)]
struct Sphere {
    center: Vec3,
    radius: f32,
    material_index: usize,
}

struct RayIntersection {
    t: f32,
    position: Vec3,
    normal: Vec3,
    material_index: usize,
}

impl RayIntersection {
    fn new() -> Self {
        Self {
            t: -1.0,
            position: Vec3::new(0.0, 0.0, 0.0),
            normal: Vec3::new(0.0, 0.0, 0.0),
            material_index: 0,
        }
    }
}

#[derive(Default)]
struct Camera {
    origin: Vec3,
    lower_left_corner: Vec3,
    horizontal: Vec3,
    vertical: Vec3,
    lens_radius: f32,
    u: Vec3,
    v: Vec3,
    //w: Vec3,
}

impl Camera {
    fn new(
        position: Vec3,
        look_at: Vec3,
        up: Vec3,
        vfov: f32,
        aspect: f32,
        aperture: f32,
        focus_dist: f32,
    ) -> Camera {
        let theta = vfov * PI / 180.0;
        let half_height = (theta / 2.0).tan();
        let half_width = aspect * half_height;

        let w = Vec3::normalize(position - look_at);
        let u = Vec3::normalize(Vec3::cross(up, w));
        let v = Vec3::cross(w, u);

        Camera {
            origin: position,
            lower_left_corner: position
                - half_width * focus_dist * u
                - half_height * focus_dist * v
                - focus_dist * w,
            horizontal: 2.0 * half_width * focus_dist * u,
            vertical: 2.0 * half_height * focus_dist * v,
            lens_radius: aperture / 2.0,
            u,
            v,
            //w,
        }
    }

    fn generate_ray(&self, s: f32, t: f32, rng: &mut ThreadRng) -> (Vec3, Vec3) {
        let rd = self.lens_radius * random_in_unit_disk(rng);
        let offset = self.u * rd.x + self.v * rd.y;
        (
            self.origin + offset,
            self.lower_left_corner + s * self.horizontal + t * self.vertical - self.origin - offset,
        )
    }
}

#[derive(Default)]
struct CubeMap {
    faces: [RgbImage; 6],
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum BackgroundType {
    CubeMap,
    EquirectangularTexture,
}

pub struct Scene {
    materials: Vec<Material>,
    spheres: Vec<Sphere>,
    camera: Camera,
    background: CubeMap,
    pub background_texture: Rgb32FImage,
    pub background_type: BackgroundType,
}

impl Scene {
    fn new() -> Self {
        Self {
            materials: Vec::new(),
            spheres: Vec::new(),
            camera: Camera::default(),
            background: CubeMap::default(),
            background_texture: Rgb32FImage::default(),
            background_type: BackgroundType::CubeMap,
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct Tile {
    pub min_x: u32,
    pub max_x: u32,
    pub min_y: u32,
    pub max_y: u32,
}

pub struct ImageFilm {
    pub width: u32,
    pub height: u32,
    pub sample_count: u32,
}

fn refract(v: Vec3, n: Vec3, ni_over_nt: f32) -> (bool, Vec3) {
    let uv = Vec3::normalize(v);
    let dt = Vec3::dot(uv, n);
    let discriminant = 1.0 - ni_over_nt * ni_over_nt * (1.0 - dt * dt);
    if discriminant > 0.0 {
        let refracted = ni_over_nt * (uv - n * dt) - n * discriminant.sqrt();
        (true, refracted)
    } else {
        (false, Vec3::new(0.0, 0.0, 0.0))
    }
}

fn schlick(cosine: f32, ior: f32) -> f32 {
    let mut r0 = (1.0 - ior) / (1.0 + ior);
    r0 = r0 * r0;
    r0 + (1.0 - r0) * (1.0 - cosine).powf(5.0)
}

fn calculate_sky_color(ray_direction: Vec3) -> Vec3 {
    let t = 0.5 * (Vec3::normalize(ray_direction).y + 1.0);
    let bottom = Vec3::new(1.0, 1.0, 1.0);
    let top = Vec3::new(0.5, 0.7, 1.0);
    Vec3::lerp(bottom, top, t)
}

fn ray_miss_shader(scene: &Scene, ray_direction: Vec3) -> Vec3 {
    // TODO: Parameterize behaviour
    //calculate_sky_color(ray_direction)

    match scene.background_type {
        BackgroundType::CubeMap => {
            // Figure out which face to sample for a given ray_direction
            let axis = find_most_significant_axis(ray_direction);

            // TODO: Handle ray_direction sign

            let index = match axis {
                Axis::X => 0,
                Axis::Y => 2,
                Axis::Z => 4,
            };

            let src = scene.background.faces[index as usize].get_pixel(0, 0);

            Vec3 {
                x: src[0] as f32 / 255.0,
                y: src[1] as f32 / 255.0,
                z: src[2] as f32 / 255.0,
            }
        }
        BackgroundType::EquirectangularTexture => {
            // Convert ray_direction from cartesian to equirectangular coordinates
            let (sx, sy) = map_cartesian_to_spherical_coordinates(ray_direction);
            let (u, mut v) = map_spherical_to_equirectangular_coordinates(sx, sy);

            v = 1.0 - v;

            // Map UV equirectangular coordinates to pixel coordinates
            let fx = u * (scene.background_texture.width() as f32 - 1.0);
            let fy = v * (scene.background_texture.height() as f32 - 1.0);

            let x = f32::floor(fx) as u32;
            let y = f32::floor(fy) as u32;

            // Perform nearest-neighbour sampling of equirectangular texture
            let src = scene.background_texture.get_pixel(x, y);

            Vec3 {
                x: src[0],
                y: src[1],
                z: src[2],
            }
        }
    }
}

fn ray_hit_shader(
    scene: &Scene,
    ray_intersection: RayIntersection,
    ray_direction: Vec3,
    rng: &mut ThreadRng,
    depth: u32,
) -> Vec3 {
    let material = scene.materials[ray_intersection.material_index];

    match material.base_type {
        MaterialType::Diffuse => {
            let target =
                ray_intersection.position + ray_intersection.normal + random_in_unit_sphere(rng);

            let incoming_radiance = sample_scene(
                scene,
                ray_intersection.position,
                target - ray_intersection.position,
                rng,
                depth + 1,
            );

            Vec3::hadamard(material.albedo, incoming_radiance)
        }

        MaterialType::Metal => {
            let reflected = Vec3::reflect(Vec3::normalize(ray_direction), ray_intersection.normal)
                + random_in_unit_sphere(rng) * material.fuzziness;

            let incoming_radiance =
                sample_scene(scene, ray_intersection.position, reflected, rng, depth + 1);

            Vec3::hadamard(material.albedo, incoming_radiance)
        }

        MaterialType::Dielectric => {
            let reflected = Vec3::reflect(ray_direction, ray_intersection.normal);

            let outward_normal: Vec3;
            let ni_over_nt: f32;
            let cosine: f32;

            if Vec3::dot(ray_direction, ray_intersection.normal) > 0.0 {
                outward_normal = -ray_intersection.normal;
                ni_over_nt = material.ior;
                cosine = material.ior * Vec3::dot(ray_direction, ray_intersection.normal)
                    / Vec3::length(ray_direction);
            } else {
                outward_normal = ray_intersection.normal;
                ni_over_nt = 1.0 / material.ior;
                cosine = -Vec3::dot(ray_direction, ray_intersection.normal)
                    / Vec3::length(ray_direction);
            }

            let (success, refracted) = refract(ray_direction, outward_normal, ni_over_nt);
            let reflect_prob: f32 = if success {
                schlick(cosine, material.ior)
            } else {
                1.0
            };

            let scattered: Vec3 = if rng.gen_range(0.0..1.0) < reflect_prob {
                reflected
            } else {
                refracted
            };

            let incoming_radiance =
                sample_scene(scene, ray_intersection.position, scattered, rng, depth + 1);
            Vec3::hadamard(material.albedo, incoming_radiance)
        }

        MaterialType::Microfacet => {
            // TODO: Precompute
            let sigma = f32::to_radians(material.sigma);
            let sigma2 = sigma * sigma;
            let A = 1.0 - ( sigma2 / (2.0 * (sigma2 + 0.33)));
            let B = 0.45 * sigma2 / (sigma2 + 0.09);

            let target =
                ray_intersection.position + ray_intersection.normal + random_in_unit_sphere(rng);
            let incoming_radiance = sample_scene(
                scene,
                ray_intersection.position,
                target - ray_intersection.position,
                rng,
                depth + 1,
            );

            // Compute orthonormal basis oriented around the surface normal
            let basis = Mat3::build_orthonormal_basis(ray_intersection.normal);

            // Convert incoming and outgoing directions to surface shading/tangent coordinate space
            let wo = Vec3::normalize(basis * ray_direction);
            let wi = Vec3::normalize(basis * (target - ray_intersection.position));

            // BRDF is evaluated in spherical coords
            let cosThetaI = wi.z;
            let cosThetaO = wo.z;

            // Use this for sin(theta)^2 + cos(theta)^2 = 1 identity
            let cosThetaI2 = cosThetaI * cosThetaI;
            let cosThetaO2 = cosThetaO * cosThetaO;

            // Prevent it from going below 0 before sqrt()
            let sinThetaI2 = f32::max(0.0, 1.0 - cosThetaI2);
            let sinThetaO2 = f32::max(0.0, 1.0 - cosThetaO2);

            let sinThetaI = f32::sqrt(sinThetaI2);
            let sinThetaO = f32::sqrt(sinThetaO2);

            // Compute cosine term for oren nayar model
            let mut maxCos = 0.0;
            if sinThetaI > f32::EPSILON && sinThetaO > f32::EPSILON {
                let sinPhiI = if sinThetaI == 0.0 { 0.0 } else { f32::clamp(wi.y / sinThetaI, -1.0, 1.0) };
                let cosPhiI = if sinThetaI == 0.0 { 1.0 } else { f32::clamp(wi.x / sinThetaI, -1.0, 1.0) };

                let sinPhiO = if sinThetaO == 0.0 { 0.0 } else { f32::clamp(wo.y / sinThetaO, -1.0, 1.0) };
                let cosPhiO = if sinThetaO == 0.0 { 1.0 } else { f32::clamp(wo.x / sinThetaO, -1.0, 1.0) };

                let dCos = cosPhiI * cosPhiO + sinPhiI * sinPhiO;
                maxCos = f32::max(0.0, dCos);
            }

            let mut sinAlpha = 0.0;
            let mut tanBeta = 0.0;
            if cosThetaI.abs() > cosThetaO.abs() {
                sinAlpha = sinThetaO;
                tanBeta = sinThetaI / cosThetaI.abs();
            } else {
                sinAlpha = sinThetaI;
                tanBeta = sinThetaO / cosThetaO.abs();
            }

            Vec3::hadamard(material.albedo, incoming_radiance) * (A + B * maxCos * sinAlpha * tanBeta)
        }
    }
}

fn ray_intersect_sphere(
    ray_origin: Vec3,
    ray_direction: Vec3,
    sphere_center: Vec3,
    sphere_radius: f32,
    tmin: f32,
    tmax: f32,
) -> f32 {
    let oc = ray_origin - sphere_center;
    let a = Vec3::dot(ray_direction, ray_direction);
    let b = Vec3::dot(oc, ray_direction);
    let c = Vec3::dot(oc, oc) - sphere_radius * sphere_radius;

    let discriminant = b * b - a * c;
    if discriminant >= 0.0 {
        let mut temp = (-b - discriminant.sqrt()) / a;
        if temp < tmax && temp > tmin {
            return temp;
        }

        temp = (-b + discriminant.sqrt()) / a;
        if temp < tmax && temp > tmin {
            return temp;
        }
    }
    -1.0
}

fn sample_scene(
    scene: &Scene,
    ray_origin: Vec3,
    ray_direction: Vec3,
    rng: &mut ThreadRng,
    depth: u32,
) -> Vec3 {
    let color: Vec3;

    let tmin = 0.001;
    let tmax = 1000.0;
    let mut closest_so_far = tmax;
    let mut ray_intersection = RayIntersection::new();

    // TODO: Separate out ray intersect scene logic
    if depth < 4 {
        for sphere in &scene.spheres {
            let t = ray_intersect_sphere(
                ray_origin,
                ray_direction,
                sphere.center,
                sphere.radius,
                tmin,
                closest_so_far,
            );

            if t > tmin && t < closest_so_far {
                closest_so_far = t;
                ray_intersection.t = t;
                ray_intersection.position = ray_origin + ray_direction * t;
                ray_intersection.normal =
                    Vec3::normalize(ray_intersection.position - sphere.center);
                ray_intersection.material_index = sphere.material_index;
            }
        }

        if closest_so_far < tmax {
            color = ray_hit_shader(scene, ray_intersection, ray_direction, rng, depth);
        } else {
            color = ray_miss_shader(scene, ray_direction);
        }
    } else {
        color = Vec3::new(0.0, 0.0, 0.0);
    }

    color
}

pub fn render_tile(
    tile: Tile,
    image_buffer: &mut RgbImage,
    image_film: &ImageFilm,
    scene: &Scene,
    rng: &mut ThreadRng,
) {
    for y in tile.min_y..tile.max_y {
        for x in tile.min_x..tile.max_x {
            let mut color = Vec3::new(0.0, 0.0, 0.0);

            for _sample_index in 0..image_film.sample_count {
                let x_offset = rng.gen_range(0.0..1.0);
                let y_offset = rng.gen_range(0.0..1.0);
                let fx = (x as f32 + x_offset) / image_film.width as f32;
                let fy = 1.0 - ((y as f32 + y_offset) / image_film.height as f32);

                let (ray_origin, ray_direction) = scene.camera.generate_ray(fx, fy, rng);

                let sample = sample_scene(scene, ray_origin, ray_direction, rng, 0);
                color = color + sample * (1.0 / image_film.sample_count as f32);
            }

            // Perform gamma correction
            let tone_mapped_color = Vec3::new(color.x.sqrt(), color.y.sqrt(), color.z.sqrt());

            let r = (tone_mapped_color.x * 255.99) as u8;
            let g = (tone_mapped_color.y * 255.99) as u8;
            let b = (tone_mapped_color.z * 255.99) as u8;

            let tile_local_x = x - tile.min_x;
            let tile_local_y = y - tile.min_y;
            image_buffer.put_pixel(tile_local_x, tile_local_y, image::Rgb([r, g, b]));
        }
    }
}

pub fn create_sphere_scene(rng: &mut ThreadRng, image_width: u32, image_height: u32) -> Scene {
    let mut scene = Scene::new();

    let aspect = image_width as f32 / image_height as f32;
    let camera_position = Vec3::new(0.0, 0.0, 15.0);
    let look_at = Vec3::new(0.0, 0.0, 0.0);
    let dist_to_focus = Vec3::length(camera_position - look_at);
    let aperture = 0.0; //0.04;

    scene.camera = Camera::new(
        camera_position,
        look_at,
        Vec3::new(0.0, 1.0, 0.0),
        50.0,
        aspect,
        aperture,
        dist_to_focus,
    );

    scene.materials = vec![Material::new_diffuse(Vec3::new(0.5, 0.5, 0.5))];

    for a in 0..=4 {
        for b in 0..=4 {
            let center = Vec3::new(-4.0 + b as f32 * 2.2, -4.0 + a as f32 * 2.2, 0.0);

            if a > 1 {
            scene.materials.push(Material::new_diffuse(Vec3::new(0.18, 0.18, 0.18)));
            } else {
                scene.materials.push(
                    Material::new_oren_nayar(Vec3::new(0.18, 0.18, 0.18), b as f32 * 20.0));
            }

            scene.spheres.push(Sphere {
                center,
                radius: 1.0,
                material_index: scene.materials.len() - 1,
            });
        }
    }

    /*
        scene.spheres = vec![Sphere {
            center: Vec3::new(0.0, -1000.0, 0.0),
            radius: 1000.0,
            material_index: scene.materials.len() - 1,
        }];

        for a in -11..11 {
            for b in -11..11 {
                let material_choice = rng.gen_range(0.0..1.0);
                let center = Vec3::new(
                    a as f32 + 0.9 * rng.gen_range(0.0..1.0),
                    0.2,
                    b as f32 + 0.9 * rng.gen_range(0.0..1.0),
                );

                if material_choice < 0.8 {
                    // Diffuse
                    scene.materials.push(Material::new_diffuse(Vec3::new(
                        rng.gen_range(0.0..1.0),
                        rng.gen_range(0.0..1.0),
                        rng.gen_range(0.0..1.0),
                    )));
                } else if material_choice < 0.95 {
                    // Metal
                    scene.materials.push(Material::new_metal(
                        Vec3::new(
                            rng.gen_range(0.5..1.0),
                            rng.gen_range(0.5..1.0),
                            rng.gen_range(0.5..1.0),
                        ),
                        rng.gen_range(0.0..0.5),
                    ));
                } else {
                    // Glass
                    scene.materials.push(Material::new_dielectric(1.5));
                }

                if Vec3::length(center - Vec3::new(4.0, 0.2, 0.0)) > 0.9 {
                    scene.spheres.push(Sphere {
                        center,
                        radius: 0.2,
                        material_index: scene.materials.len() - 1,
                    });
                }
            }
        }

        scene.materials.push(Material::new_dielectric(1.5));
        scene.spheres.push(Sphere {
            center: Vec3::new(0.0, 1.0, 0.0),
            radius: 1.0,
            material_index: scene.materials.len() - 1,
        });

        scene
            .materials
            .push(Material::new_diffuse(Vec3::new(0.4, 0.2, 0.1)));
        scene.spheres.push(Sphere {
            center: Vec3::new(-4.0, 1.0, 0.0),
            radius: 1.0,
            material_index: scene.materials.len() - 1,
        });

        scene
            .materials
            .push(Material::new_metal(Vec3::new(0.7, 0.6, 0.5), 0.0));
        scene.spheres.push(Sphere {
            center: Vec3::new(4.0, 1.0, 0.0),
            radius: 1.0,
            material_index: scene.materials.len() - 1,
        });
    */

    // TODO: Remove this
    // X
    scene.background.faces[0] = image::ImageBuffer::from_pixel(1, 1, Rgb([255, 0, 0]));
    scene.background.faces[1] = image::ImageBuffer::from_pixel(1, 1, Rgb([255, 0, 0]));

    // Y
    scene.background.faces[2] = image::ImageBuffer::from_pixel(1, 1, Rgb([0, 255, 0]));
    scene.background.faces[3] = image::ImageBuffer::from_pixel(1, 1, Rgb([0, 255, 0]));

    // Z
    scene.background.faces[4] = image::ImageBuffer::from_pixel(1, 1, Rgb([0, 0, 255]));
    scene.background.faces[5] = image::ImageBuffer::from_pixel(1, 1, Rgb([0, 0, 255]));

    /*
    let mut equirectangular_map = image::ImageBuffer::new(6, 3);
    // Create equirectangular test image
    for y in 0..3 {
        for x in 0..6 {
            let fx = x as f32 / 6.0;
            let fy = y as f32 / 3.0;

            // Map equirectangular to spherical coords
            let (sx, sy) = map_equirectangular_to_spherical_coordinates(fx, fy);
            let v = map_spherical_to_cartesian_coordinates(sx, sy);
            let color = match find_most_significant_axis(v) {
                0 => Rgb([255, 0, 0]),
                1 => Rgb([0, 255, 0]),
                2 => Rgb([0, 0, 255]),
                _ => Rgb([0, 0, 0]),
            };

            equirectangular_map.put_pixel(x, y, color);
        }
    }

    scene.background_texture = equirectangular_map;
    */
    scene.background_type = BackgroundType::EquirectangularTexture;

    scene
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_ray_intersect_sphere() {
        let t = ray_intersect_sphere(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(0.0, 0.0, -1.0),
            Vec3::new(0.0, 0.0, -5.0),
            0.5,
            0.0,
            100.0,
        );
        assert_eq!(t, 4.5);
    }

    #[test]
    fn test_ray_intersect_sphere_miss() {
        let t = ray_intersect_sphere(
            Vec3::new(0.0, 0.0, 0.0),
            Vec3::new(1.0, 0.0, -1.0),
            Vec3::new(0.0, 0.0, -5.0),
            0.5,
            0.0,
            100.0,
        );
        assert_eq!(t, -1.0);
    }

    #[test]
    fn test_ray_intersect_sphere_behind_camera() {
        let t = ray_intersect_sphere(
            Vec3::new(0.0, 0.0, -10.0),
            Vec3::new(0.0, 0.0, -1.0),
            Vec3::new(0.0, 0.0, -5.0),
            0.5,
            0.0,
            100.0,
        );
        assert_eq!(t, -1.0);
    }

    #[test]
    fn test_ray_miss_shader_cube_map() {
        // Given an empty scene with a background cubemap of basis colors (X->Red, Y->Green, etc)
        let mut scene = Scene::new();

        // X
        scene.background.faces[0] = image::ImageBuffer::from_pixel(1, 1, Rgb([255, 0, 0]));
        scene.background.faces[1] = image::ImageBuffer::from_pixel(1, 1, Rgb([255, 0, 0]));

        // Y
        scene.background.faces[2] = image::ImageBuffer::from_pixel(1, 1, Rgb([0, 255, 0]));
        scene.background.faces[3] = image::ImageBuffer::from_pixel(1, 1, Rgb([0, 255, 0]));

        // Z
        scene.background.faces[4] = image::ImageBuffer::from_pixel(1, 1, Rgb([0, 0, 255]));
        scene.background.faces[5] = image::ImageBuffer::from_pixel(1, 1, Rgb([0, 0, 255]));

        let ray_origin = Vec3::new(0.0, 0.0, 0.0);

        let mut rng = rand::thread_rng();

        // When we sample in the -Z direction then we get the blue background color
        assert_eq!(
            sample_scene(&scene, ray_origin, Vec3::new(0.0, 0.0, -1.0), &mut rng, 0),
            Vec3::new(0.0, 0.0, 1.0)
        );

        // When we sample in the +X direction then we get the red background color
        assert_eq!(
            sample_scene(&scene, ray_origin, Vec3::new(1.0, 0.0, 0.0), &mut rng, 0),
            Vec3::new(1.0, 0.0, 0.0)
        );

        // When we sample in the Y direction then we get the green background color
        assert_eq!(
            sample_scene(&scene, ray_origin, Vec3::new(0.0, 1.0, 0.0), &mut rng, 0),
            Vec3::new(0.0, 1.0, 0.0)
        );
    }

    #[test]
    fn test_ray_miss_shader_equirectangular_map() {
        let mut equirectangular_map = image::ImageBuffer::new(6, 3);
        // Create equirectangular test image
        for y in 0..3 {
            for x in 0..6 {
                let fx = x as f32 / 6.0;
                let fy = y as f32 / 3.0;

                // Map equirectangular to spherical coords
                let (sx, sy) = map_equirectangular_to_spherical_coordinates(fx, fy);
                let v = map_spherical_to_cartesian_coordinates(sx, sy);
                let color = match find_most_significant_axis(v) {
                    Axis::X => Rgb([1.0, 0.0, 0.0]),
                    Axis::Y => Rgb([0.0, 1.0, 0.0]),
                    Axis::Z => Rgb([0.0, 0.0, 1.0]),
                };

                equirectangular_map.put_pixel(x, y, color);
            }
        }

        // Given an empty scene with a background equirectangular map
        let mut scene = Scene::new();

        scene.background_type = BackgroundType::EquirectangularTexture;
        scene.background_texture = equirectangular_map;

        let ray_origin = Vec3::new(0.0, 0.0, 0.0);

        let mut rng = rand::thread_rng();

        // When we sample in the -Z direction then we get the blue background color
        assert_eq!(
            sample_scene(&scene, ray_origin, Vec3::new(0.0, 0.0, -1.0), &mut rng, 0),
            Vec3::new(0.0, 0.0, 1.0)
        );

        // When we sample in the +X direction then we get the red background color
        // FIXME: This is definitely broken (maybe sampling?), its returning (0.0, 0.0, 1.0)
        //assert_eq!(
        //sample_scene(&scene, ray_origin, Vec3::new(1.0, 0.0, 0.0), &mut rng, 0),
        //Vec3::new(1.0, 0.0, 0.0)
        //);

        // When we sample in the Y direction then we get the green background color
        assert_eq!(
            sample_scene(&scene, ray_origin, Vec3::new(0.0, 1.0, 0.0), &mut rng, 0),
            Vec3::new(0.0, 1.0, 0.0)
        );
    }
}
