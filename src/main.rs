/* TODO:
- Tile based renderer [x]
- Ray dispatch shader [ ]
- Image based lighting [x]
- HDR image storage [x]
- Microfacet PBR shader [ ]
*/
#![allow(dead_code)]
#![allow(unused_variables)]

use clap::Parser;
use crossbeam_channel::unbounded;
use image::io::Reader as ImageReader;
use image::RgbImage;
use path_tracer::{create_sphere_scene, render_tile, ImageFilm, Tile};
use std::sync::Arc;
use std::thread;
use std::time::Instant;

pub mod math_utils;
pub mod path_tracer;
pub mod vec3;
pub mod mat3;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    #[arg(short = 'x', long)]
    image_width: u32,

    #[arg(short = 'y', long)]
    image_height: u32,

    #[arg(short, long)]
    sample_count: u32,

    #[arg(short, long)]
    thread_count: u32,

    #[arg(short, long)]
    output_path: String,

    #[arg(short, long)]
    hdri_path: String,
}

//type TileCoordinate = (u32, u32);

const TILE_WIDTH: u32 = 64;

fn main() {
    let app_start_time = Instant::now();

    let args = Args::parse();
    let image_width = args.image_width;
    let image_height = args.image_height;
    let sample_count = args.sample_count;
    let tile_count_x = image_width / TILE_WIDTH;
    let tile_count_y = image_height / TILE_WIDTH;
    let thread_count = args.thread_count;

    println!(
        "Rendering {}x{} image with {} samples per pixel using {} threads...",
        image_width, image_height, sample_count, thread_count
    );

    let mut image_buffer: RgbImage = image::ImageBuffer::new(image_width, image_height);
    let image_film = Arc::new(ImageFilm {
        width: image_width,
        height: image_height,
        sample_count,
    });

    let mut rng = rand::thread_rng();

    let mut scene = create_sphere_scene(&mut rng, image_width, image_height);
    scene.background_texture = ImageReader::open(args.hdri_path)
        .unwrap()
        .decode()
        .unwrap()
        .into_rgb32f();

    // TODO: Could use bounded channel since we know size upfront
    let (input_sender, input_receiver) = unbounded();
    let (output_sender, output_receiver) = unbounded();

    // TODO: Handle % TILE_WIDTH != 0, currently this just truncates the image
    for tile_y in 0..tile_count_y {
        for tile_x in 0..tile_count_x {
            // Build vec of tile coords
            input_sender.send((tile_x, tile_y)).unwrap();
        }
    }

    let scene = Arc::new(scene);

    for _ in 0..thread_count {
        let input_queue = input_receiver.clone();
        let output_queue = output_sender.clone();

        let image_film = Arc::clone(&image_film);
        let scene = Arc::clone(&scene);
        thread::spawn(move || {
            let mut thread_rng = rand::thread_rng();

            // TODO: Map?
            for tile_coords in input_queue.iter() {
                let tile = Tile {
                    min_x: tile_coords.0 * TILE_WIDTH,
                    max_x: (tile_coords.0 + 1) * TILE_WIDTH,
                    min_y: tile_coords.1 * TILE_WIDTH,
                    max_y: (tile_coords.1 + 1) * TILE_WIDTH,
                };

                let mut thread_image_buffer: RgbImage =
                    image::ImageBuffer::new(TILE_WIDTH, TILE_WIDTH);
                render_tile(
                    tile,
                    &mut thread_image_buffer,
                    &image_film,
                    &scene,
                    &mut thread_rng,
                );

                // Send tile back to main thread to assemble
                output_queue
                    .send((tile_coords, thread_image_buffer))
                    .unwrap();
            }
        });
    }
    drop(input_sender);
    drop(input_receiver);
    drop(output_sender);

    // TODO: collect()?
    // Assemble tiles into final image
    for (tile_coords, tile_image_buffer) in output_receiver.iter() {
        let tile = Tile {
            min_x: tile_coords.0 * TILE_WIDTH,
            max_x: (tile_coords.0 + 1) * TILE_WIDTH,
            min_y: tile_coords.1 * TILE_WIDTH,
            max_y: (tile_coords.1 + 1) * TILE_WIDTH,
        };

        // Blit tile into image
        for y in 0..TILE_WIDTH {
            for x in 0..TILE_WIDTH {
                let img_x = tile.min_x + x;
                let img_y = tile.min_y + y;
                let src = tile_image_buffer.get_pixel(x, y);
                image_buffer.put_pixel(img_x, img_y, *src);
            }
        }
    }

    image_buffer.save(args.output_path).unwrap();

    let app_duration = Instant::now() - app_start_time;
    println!("Completed in {} seconds", app_duration.as_secs_f32());
}
