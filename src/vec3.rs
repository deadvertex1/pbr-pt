use core::ops::{Add, Mul, Neg, Sub};

#[derive(Debug, Copy, Clone, PartialEq, Default)]
pub struct Vec3 {
    pub x: f32,
    pub y: f32,
    pub z: f32,
}

impl Vec3 {
    pub fn new(_x: f32, _y: f32, _z: f32) -> Vec3 {
        Vec3 {
            x: _x,
            y: _y,
            z: _z,
        }
    }

    pub fn lerp(a: Vec3, b: Vec3, t: f32) -> Vec3 {
        a * (1.0 - t) + b * t
    }

    pub fn dot(a: Vec3, b: Vec3) -> f32 {
        a.x * b.x + a.y * b.y + a.z * b.z
    }

    pub fn length(v: Vec3) -> f32 {
        Self::dot(v, v).sqrt()
    }

    pub fn normalize(v: Vec3) -> Vec3 {
        let length = Self::length(v);
        v * (1.0 / length)
    }

    pub fn reflect(v: Vec3, n: Vec3) -> Vec3 {
        v - n * 2.0 * Self::dot(v, n)
    }

    pub fn hadamard(a: Vec3, b: Vec3) -> Vec3 {
        Vec3 {
            x: a.x * b.x,
            y: a.y * b.y,
            z: a.z * b.z,
        }
    }

    pub fn cross(a: Vec3, b: Vec3) -> Vec3 {
        Vec3 {
            x: (a.y * b.z) - (a.z * b.y),
            y: (a.z * b.x) - (a.x * b.z),
            z: (a.x * b.y) - (a.y * b.x),
        }
    }
}

impl Add for Vec3 {
    type Output = Self;

    fn add(self, other: Vec3) -> Vec3 {
        Self {
            x: self.x + other.x,
            y: self.y + other.y,
            z: self.z + other.z,
        }
    }
}

impl Neg for Vec3 {
    type Output = Self;

    fn neg(self) -> Vec3 {
        Self {
            x: -self.x,
            y: -self.y,
            z: -self.z,
        }
    }
}

impl Sub for Vec3 {
    type Output = Self;

    fn sub(self, other: Vec3) -> Vec3 {
        Self {
            x: self.x - other.x,
            y: self.y - other.y,
            z: self.z - other.z,
        }
    }
}

impl Mul<f32> for Vec3 {
    type Output = Self;

    fn mul(self, s: f32) -> Vec3 {
        Self {
            x: self.x * s,
            y: self.y * s,
            z: self.z * s,
        }
    }
}

impl Mul<Vec3> for f32 {
    type Output = Vec3;

    fn mul(self, v: Vec3) -> Vec3 {
        Vec3 {
            x: v.x * self,
            y: v.y * self,
            z: v.z * self,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vec3_new() {
        let v = Vec3::new(1.0, 2.0, 3.0);
        assert_eq!(v.x, 1.0);
        assert_eq!(v.y, 2.0);
        assert_eq!(v.z, 3.0);
    }

    #[test]
    fn test_vec3_add() {
        let v = Vec3::new(1.0, 2.0, 3.0) + Vec3::new(4.0, 5.0, 6.0);
        assert_eq!(v.x, 5.0);
        assert_eq!(v.y, 7.0);
        assert_eq!(v.z, 9.0);
    }

    #[test]
    fn test_vec3_sub() {
        let v = Vec3::new(4.0, 5.0, 6.0) - Vec3::new(1.0, 2.0, 3.0);
        assert_eq!(v.x, 3.0);
        assert_eq!(v.y, 3.0);
        assert_eq!(v.z, 3.0);
    }

    #[test]
    fn test_vec3_mul() {
        let v = Vec3::new(1.0, 2.0, 3.0) * 2.0;
        assert_eq!(v.x, 2.0);
        assert_eq!(v.y, 4.0);
        assert_eq!(v.z, 6.0);
    }

    #[test]
    fn test_vec3_neg() {
        let v = -Vec3::new(1.0, 2.0, 3.0);
        assert_eq!(v.x, -1.0);
        assert_eq!(v.y, -2.0);
        assert_eq!(v.z, -3.0);
    }

    #[test]
    fn test_vec3_eq() {
        let a = Vec3::new(1.0, 2.0, 3.0);
        assert_eq!(a, a);
    }

    #[test]
    fn test_vec3_neq() {
        let a = Vec3::new(1.0, 2.0, 3.0);
        let b = Vec3::new(2.0, 3.0, 4.0);
        assert!(a != b);
    }

    #[test]
    fn test_vec3_lerp() {
        assert_eq!(
            Vec3::lerp(Vec3::new(1.0, 2.0, 3.0), Vec3::new(2.0, 3.0, 4.0), 0.0),
            Vec3::new(1.0, 2.0, 3.0)
        );

        assert_eq!(
            Vec3::lerp(Vec3::new(1.0, 2.0, 3.0), Vec3::new(2.0, 3.0, 4.0), 1.0),
            Vec3::new(2.0, 3.0, 4.0)
        );

        assert_eq!(
            Vec3::lerp(Vec3::new(1.0, 2.0, 3.0), Vec3::new(2.0, 3.0, 4.0), 0.5),
            Vec3::new(1.5, 2.5, 3.5)
        );
    }

    #[test]
    fn test_vec3_dot() {
        assert_eq!(
            Vec3::dot(Vec3::new(1.0, 2.0, 3.0), Vec3::new(4.0, 5.0, 6.0)),
            32.0
        );
    }

    #[test]
    fn test_vec3_length() {
        assert_eq!(Vec3::length(Vec3::new(1.0, 2.0, 3.0)), 14.0_f32.sqrt());
    }

    #[test]
    fn test_vec3_normalize() {
        assert_eq!(
            Vec3::normalize(Vec3::new(0.0, 10.0, 0.0)),
            Vec3::new(0.0, 1.0, 0.0)
        );
    }

    #[test]
    fn test_vec3_reflect() {
        assert_eq!(
            Vec3::reflect(Vec3::new(1.0, -1.0, 0.0), Vec3::new(0.0, 1.0, 0.0)),
            Vec3::new(1.0, 1.0, 0.0)
        );
    }

    #[test]
    fn test_vec3_hadamard() {
        assert_eq!(
            Vec3::hadamard(Vec3::new(1.0, 2.0, 3.0), Vec3::new(2.0, 3.0, 4.0)),
            Vec3::new(2.0, 6.0, 12.0)
        );
    }
}
