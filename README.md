# PBR PT

Rust implementation of a Physically Based Rendering style Path Tracer started
from following [Ray Tracing in One Weekend by Peter
Shirley](https://www.realtimerendering.com/raytracing/Ray%20Tracing%20in%20a%20Weekend.pdf)

## Screenshots

![6-11-2022.png](/screenshots/6-11-2022.png)
